import json

from classes import Challenge, Lesson, StudentSubmission

file = open("exploration.json")
data = json.load(file)

file = open("users.json")
user_data = json.load(file)


def get_student_completion_rate_by_email(exploration, users):
    result = get_student_completion_rate(exploration)

    for user in users:
        id = str(user["id"])
        email = user["email"]
        result[email] = result.get(id)
        if result.get(id):
            del result[id]
    return result


def get_student_completion_rate(exploration):
    student_responses = get_student_responses(exploration)
    result = {}
    for student, responses in student_responses.items():
        total = len(responses)
        correct_responses = sum(
            response.status == "correct" for response in responses
        )
        percentage_correct = round(correct_responses / total * 100)
        result[student] = {
            "total": total,
            "correct responses": correct_responses,
            "percentage correct": f"{percentage_correct}%",
        }
    return result


def get_student_responses(exploration):
    result = {}
    for lesson in exploration:
        lesson = Lesson(lesson)
        for challenge in lesson.challenges:
            for id, submission in challenge.submissions.items():
                if id in result.keys():
                    result[id].append(
                        {"title": challenge.title, "status": submission.status}
                    )
                else:
                    result[id] = [
                        {"title": challenge.title, "status": submission.status}
                    ]
    return result


print(json.dumps(get_student_completion_rate_by_email(data, user_data), indent=2))
