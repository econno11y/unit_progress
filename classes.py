from typing import Dict, List



class StudentSubmission:
    def __init__(self, object):
        self.status: str = object["status"]
        self.correct_points: str = object["correct_points"]
        self.total_points: str = object["total_points"]



class Challenge:
    def __init__(self, challenge):
        self.id: int = challenge["id"]
        self.title: str = challenge["title"]
        self.submissions: Dict[str, StudentSubmission] = {
            key: StudentSubmission(value)
            for key, value in challenge.get("student_submissions").items()
        }



class Lesson:
    def __init__(self, lesson):
        self.id: int = lesson["id"]
        self.title: str = lesson["title"]
        self.challenges: List[Challenge] = [
            Challenge(challenge) for challenge in lesson["challenges"]
        ]
